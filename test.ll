declare i32 @puts (i8*)

@global_str = constant [13 x i8] c"Hello World!\00"
 
define	i8	@ok(i8 %var) {
	%a = add i8 1, %var
	ret i8 0
}

define	i32	@main() {
	%temp = getelementptr [13 x i8], [13 x i8]* @global_str, i64 0, i64 0
	call i32 @puts(i8* %temp)

	;; SOME BASIC OPERATIONS
	%a = add i32 1, 2
	%b = mul i32 3, %a
	%c = sub i32 %a, %b
	;; CAST FROM i32 to i8
	;; 1. i32 -> <4 x i8>
	%d = bitcast i32 %c to <4 x i8>
	;; 2. <4 x i8>[0]
	%e = extractelement <4 x i8> %d, i8 0

	;; CALL ok WITH i32 CASTED (TO i8)
	%f = call i8 @ok(i8 %e)
	ret i32 0
}
